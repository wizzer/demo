# myfiles
定时生成目录和文件树
# 使用说明
启动服务时会自动运行(延迟5s)，其后按配置的间隔时间周期性执行

Docker容器，存储空间设置，添加文件夹。
被扫描文件夹可以给只读权限，保存扫描结果的文件夹需要写入权限。

# docker 环境变量说明

变量名称 | 默认值 | 说明
----|------|----
DIR | /app | 要扫描的目录多个目录以英文,分割
TO | /app | 生成的txt文件保存位置
LEVEL | 5 | 递归扫描目录的层级
SKIP | 1 | 是否跳过隐藏文件夹（0=否,1=是 跳过以 . 或 @ 开头的文件夹）
TIME | 3600  | 周期扫描的时间间隔，单位秒


# 编译说明

* `mvn package nutzboot:shade` 生成可执行jar包
* `docker build -t wizzer/myfiles:v1.0 . --platform=linux/amd64` Docker打包发布